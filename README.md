# TECNIVA TEST

## Instrucciones
Para ejecutar el proyecto es necesario tener instalado Java 17 y gradle en una versión superior a la 8.X

1. Ejecutaremos el proyecto con el siguiente comando:

$ ./gradlew bootRun

2. Dentro del proyecto hay 5 APIS que podremos probar con las siguientes consultas:

a) Obtener todos los usuarios

$ curl -X GET localhost:8080/user/all -H "Content-Type: application/json"

b) Obtener un usuario por ID

$ curl -X GET localhost:8080/user/get/{id} -H "Content-Type: application/json"

c) Crear un usuario

$ curl -X POST localhost:8080/user/create -H "Content-Type: application/json" -d '{
    "nombre": "Juan",
    "apellido": "Pérez",
    "correoElectronico": "juan.perez@example.com",
    "fechaNacimiento": "1990-01-01"
}'

d) Actualizar un usuario

$ curl -X PUT localhost:8080/user/update/{id} -H "Content-Type: application/json" -d '{
    "nombre": "Juan",
    "apellido": "Pérez",
    "correoElectronico": "juan.perez@nuevoemail.com",
    "fechaNacimiento": "1990-01-01"
}'

e) Eliminar Usuario por ID

$ curl -X DELETE localhost:8080/user/delete/{id} -H "Content-Type: application/json"

## Ejecución de pruebas unitarias

Para ejecutar las pruebas unitarias hay que correr el siguiente comando en la consola

$ ./gradlew test







