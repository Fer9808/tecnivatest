package com.tecniva.ms.service;

import java.util.List;

import com.tecniva.ms.entity.Usuario;

public interface UsuarioService {
	List<Usuario> findAll();

	Usuario findById(Long id);

	Usuario save(Usuario usuario);

	Usuario update(Long id, Usuario usuarioDetails);

	void delete(Long id);
}
