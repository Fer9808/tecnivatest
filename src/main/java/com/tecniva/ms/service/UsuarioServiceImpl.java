package com.tecniva.ms.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecniva.ms.entity.Usuario;
import com.tecniva.ms.repository.UsuarioRepository;

@Service
public class UsuarioServiceImpl implements UsuarioService {
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public List<Usuario> findAll() {
		return (List<Usuario>) usuarioRepository.findAll();
	}

	@Override
	public Usuario findById(Long id) {
		return usuarioRepository.findById(id).orElseThrow(() -> new RuntimeException("Usuario no encontrado"));
	}

	@Override
	public Usuario save(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}

	@Override
	public Usuario update(Long id, Usuario usuarioDetails) {
		Usuario usuario = findById(id);
		usuario.setNombre(usuarioDetails.getNombre());
		usuario.setApellido(usuarioDetails.getApellido());
		usuario.setCorreoElectronico(usuarioDetails.getCorreoElectronico());
		usuario.setFechaNacimiento(usuarioDetails.getFechaNacimiento());
		return usuarioRepository.save(usuario);
	}

	@Override
	public void delete(Long id) {
		Usuario usuario = findById(id);
		usuarioRepository.delete(usuario);
	}
}
