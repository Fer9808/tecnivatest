package com.tecniva.ms.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tecniva.ms.entity.Usuario;
import com.tecniva.ms.service.UsuarioService;

@RestController
@RequestMapping("user")
public class UsuarioController {
	@Autowired
	private UsuarioService usuarioService;

	@GetMapping("/all")
	public List<Usuario> getAllUsuarios() {
		return usuarioService.findAll();
	}

	@GetMapping("/get/{id}")
	public ResponseEntity<Usuario> getUsuarioById(@PathVariable(value = "id") Long id) {
		Usuario usuario = usuarioService.findById(id);
		return ResponseEntity.ok().body(usuario);
	}

	@PostMapping("/create")
	public Usuario createUsuario(@RequestBody Usuario usuario) {
		System.out.println(usuario.getNombre());
		return usuarioService.save(usuario);
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Usuario> updateUsuario(@PathVariable(value = "id") Long id,
			@RequestBody Usuario usuarioDetails) {
		Usuario updatedUsuario = usuarioService.update(id, usuarioDetails);
		return ResponseEntity.ok(updatedUsuario);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteUsuario(@PathVariable(value = "id") Long id) {
		usuarioService.delete(id);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
}
