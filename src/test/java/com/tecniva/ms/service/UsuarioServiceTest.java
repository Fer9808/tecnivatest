package com.tecniva.ms.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.tecniva.ms.entity.Usuario;
import com.tecniva.ms.repository.UsuarioRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class UsuarioServiceTest {
	@Mock
	private UsuarioRepository usuarioRepository;

	@InjectMocks
	private UsuarioService usuarioService;

	private Usuario usuario;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.openMocks(this);
		usuario = new Usuario();
		usuario.setId(1L);
		usuario.setNombre("Juan");
		// Set other properties...
	}

	@Test
    void whenFindAll_thenUsuarioList() {
        when(usuarioRepository.findAll()).thenReturn(Arrays.asList(usuario));
        List<Usuario> result = usuarioService.findAll();
        assertEquals(1, result.size());
    }

	@Test
    void whenFindById_thenUsuario() {
        when(usuarioRepository.findById(1L)).thenReturn(Optional.of(usuario));
        Usuario found = usuarioService.findById(1L);
        assertEquals(usuario, found);
    }

	@Test
    void whenSave_thenUsuario() {
        when(usuarioRepository.save(any(Usuario.class))).thenReturn(usuario);
        Usuario saved = usuarioService.save(new Usuario());
        assertNotNull(saved);
    }

	@Test
    void whenUpdate_thenUsuario() {
        when(usuarioRepository.findById(1L)).thenReturn(Optional.of(usuario));
        when(usuarioRepository.save(any(Usuario.class))).thenReturn(usuario);

        Usuario updated = usuarioService.update(1L, new Usuario());
        assertNotNull(updated);
    }

	@Test
    void whenDelete_thenVoid() {
        when(usuarioRepository.findById(1L)).thenReturn(Optional.of(usuario));
        doNothing().when(usuarioRepository).delete(any(Usuario.class));
        assertDoesNotThrow(() -> usuarioService.delete(1L));
    }
}
