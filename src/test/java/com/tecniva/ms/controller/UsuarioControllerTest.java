package com.tecniva.ms.controller;

import static org.mockito.Mockito.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tecniva.ms.entity.Usuario;
import com.tecniva.ms.service.UsuarioService;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

public class UsuarioControllerTest {

	private MockMvc mockMvc;

	@Mock
	private UsuarioService usuarioService;

	@InjectMocks
	private UsuarioController usuarioController;

	private final ObjectMapper objectMapper = new ObjectMapper();

	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);
		mockMvc = standaloneSetup(usuarioController).build();
	}

	@Test
	public void testGetAllUsuarios() throws Exception {
		given(usuarioService.findAll()).willReturn(Collections.singletonList(new Usuario()));
		mockMvc.perform(get("/api/usuarios")).andExpect(status().isOk())
				.andExpect(jsonPath("$.*", Matchers.hasSize(1))); // Usando Matchers de JUnit
	}

	@Test
	public void testGetUsuarioById() throws Exception {
		Usuario usuario = new Usuario();
		usuario.setId(1L);
		given(usuarioService.findById(1L)).willReturn(usuario);

		mockMvc.perform(get("/api/usuarios/1")).andExpect(status().isOk()).andExpect(jsonPath("$.id", Matchers.is(1)));
	}

	@Test
	public void testCreateUsuario() throws Exception {
		Usuario usuario = new Usuario();
		usuario.setNombre("Juan");
		given(usuarioService.save(any(Usuario.class))).willReturn(usuario);

		mockMvc.perform(post("/api/usuarios").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(usuario))).andExpect(status().isOk())
				.andExpect(jsonPath("$.nombre", Matchers.is("Juan")));
	}

	@Test
	public void testUpdateUsuario() throws Exception {
		Usuario usuarioActualizado = new Usuario();
		usuarioActualizado.setId(1L);
		usuarioActualizado.setNombre("Juan Actualizado");

		given(usuarioService.update(eq(1L), any(Usuario.class))).willReturn(usuarioActualizado);

		mockMvc.perform(put("/api/usuarios/1").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(usuarioActualizado))).andExpect(status().isOk())
				.andExpect(jsonPath("$.nombre", Matchers.is("Juan Actualizado")));
	}

	@Test
	public void testDeleteUsuario() throws Exception {
		doNothing().when(usuarioService).delete(1L);

		mockMvc.perform(delete("/api/usuarios/1")).andExpect(status().isOk());
	}
}
